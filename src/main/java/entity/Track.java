package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sander on 10/28/15.
 */
@Entity
@XmlRootElement
@NamedQuery(name = Track.findAll, query = "SELECT t FROM Track t")
public class Track {

    static final String PREFIX = "entity.Track.";
    public static final String findAll = PREFIX + "findAll";
    private boolean done;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 3, max = 100)
    @Column
    private String album;

    @NotNull
    @Size(min = 3, max = 100)
    @Column
    private String artist;

    @NotNull
    @Size(min = 3, max = 100)
    @Column
    private String song;

    public Track() {
    }

    public Track(String album, String artist, String song) {
        this.album = album;
        this.artist = artist;
        this.song = song;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }
}

package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by sander on 10/21/15.
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = User.findAll, query = "SELECT u FROM User u"),
        @NamedQuery(name = User.findById, query = "SELECT u From User u WHERE u.id =:Id")
}
)
public class User implements Serializable {

    static final String PREFIX = "entity.User.";
    public static final String findAll = PREFIX + "findAll";
    public static final String findById = PREFIX + "findById";
    private boolean done;

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 3, max = 20)
    @Column
    private String username;

    @NotNull
    @Size(min = 3, max = 20)
    @Column
    private String password;

    @NotNull
    @Size(min = 3, max = 50)
    @Column
    private String email;


    public User() {
    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }


}

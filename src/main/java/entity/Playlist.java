package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by sander on 10/28/15.
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = Playlist.findAll, query = "SELECT p FROM Playlist p"),
        @NamedQuery(name = Playlist.findByUserId, query = "SELECT p FROM Playlist p WHERE p.creator = :Id")
        }
)
public class Playlist implements Serializable{

    static final String PREFIX = "entity.Playlist.";
    public static final String findAll = PREFIX + "findAll";
    public static final String findByUserId = PREFIX + "findByUserId";
    private boolean done;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 3, max = 100)
    @Column
    private String name;

    @Column
    private long creator;


    @OneToMany(fetch = FetchType.EAGER)
    private List<Track> trackList;

    public Playlist() {
    }

    public Playlist(String name, User creator) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreator() {
        return creator;
    }

    public void setCreator(long creator) {
        this.creator = creator;
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
    }
}

package entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sander on 11/4/15.
 */
@XmlRootElement
public class ErrorMessage {

    private String errorMessage;
    private int errorCode;
    private String docmentationsLink;

    public ErrorMessage(){
    }

    public ErrorMessage(String errorMessage, int errorCode, String docmentationsLink) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.docmentationsLink = docmentationsLink;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getDocmentationsLink() {
        return docmentationsLink;
    }

    public void setDocmentationsLink(String docmentationsLink) {
        this.docmentationsLink = docmentationsLink;
    }
}

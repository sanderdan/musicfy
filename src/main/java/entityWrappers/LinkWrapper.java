package entityWrappers;

import utils.Link;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sander on 10/28/15.
 */
public class LinkWrapper<T> implements Serializable {

    public T entity;
    public List<Link> links = new ArrayList<>();

    public LinkWrapper<T> entity(T entity) {
        this.entity = entity;
        return this;
    }

    public LinkWrapper<T> link(Link link){
        links.add(link);
        return this;
    }

}

package service;

import entity.Playlist;
import entity.Track;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by sander on 10/26/15,
 * with inspiration from Adam Bien
 */
@Stateless
@Local(CrudService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudServiceBean implements CrudService {

    @PersistenceContext
    public EntityManager em;

    @Override
    public <T> T create(T t) {
        this.em.persist(t);
        return t;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T find(Class<T> type, Object id) {
        return this.em.find(type, id);
    }

    @Override
    public void delete(Class type, Object o) {
        try {
            Object id = this.em.getReference(type, o);
            this.em.remove(id);
        }catch (EntityNotFoundException e){
            // we want to remove it...
        }
    }

    @Override
    public <T> T update(T t) {
        return (T) this.em.merge(t);
    }

    @Override
    public List<Object> findByNamedQuery(String namedQueryName) {
        return this.em.createNamedQuery(namedQueryName).getResultList();
    }

    @Override
    public List<Object> findByNamedQueryWithId(String namedQueryName, long id) {

            return this.em.createNamedQuery(namedQueryName)
                    .setParameter("Id", id).getResultList();


    }

    @Override
    public Object findOneByNamedQueryWithId(String namedQueryName, long id) {
        return this.em.createNamedQuery(namedQueryName)
                .setParameter("Id", id).getResultList();

    }


    @Override
    public Playlist addTrackToPlayList(Playlist playlist, Track track) {
        Playlist p = em.getReference(Playlist.class, playlist.getId());
        p.getTrackList().add(track);
        em.merge(p);
        return em.merge(p);
    }

}

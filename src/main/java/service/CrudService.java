package service;

import entity.Playlist;
import entity.Track;

import java.util.List;

/**
 * Created by sander on 10/26/15.
 */
public interface CrudService {
    <T> T create(T t);

    <T> T find(Class<T> type, Object id);

    <T> T update(T t);

    void delete(Class type, Object t);

    List findByNamedQuery(String queryName);

    List findByNamedQueryWithId(String queryName, long id);

    Object findOneByNamedQueryWithId(String queryName, long id);

    Playlist addTrackToPlayList(Playlist playlist, Track track);


}

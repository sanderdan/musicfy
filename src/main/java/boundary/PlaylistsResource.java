package boundary;

import entity.Playlist;
import entity.User;
import entityWrappers.LinkWrapper;
import service.CrudService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sander on 10/28/15.
 */
@Stateless
@Path("/users/{userId}/playlists")
public class PlaylistsResource {

    private LinkWrapper<Playlist> linkWrapper;
    private LinkWrapper<User> userLinkWrapper;

    @Inject
    CrudService manager;


    @Path("{id}")
    public PlaylistResource find(@PathParam("id") long id) {
        return new PlaylistResource(id, manager);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlaylistsFromUser(@Context UriInfo uriInfo, @PathParam("userId") long id) {
        List<Playlist> playlists = manager.findByNamedQueryWithId(Playlist.findByUserId, id);
        List<LinkWrapper> linkWrappers = new ArrayList<>();

        for (Playlist p : playlists) {
            String uri = uriInfo.getBaseUriBuilder()
                    .path("/users/" + Long.toString(id) + "/playlists")
                    .path(Long.toString(p.getId()))
                    .build()
                    .toString();

            utils.Link link = new utils.Link(uri, "self");
            linkWrapper = new LinkWrapper<Playlist>().entity(p).link(link);
            linkWrappers.add(linkWrapper);
        }

        return Response.status(Response.Status.OK).entity(linkWrappers).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(@Valid Playlist playlist, @PathParam("userId") long userId, @Context UriInfo info) {
        if (playlist != null) {
            playlist.setCreator(userId);
            Playlist saved = this.manager.create(playlist);
            saved.setId(playlist.getId());
            URI uri = info.getAbsolutePathBuilder().path("/" + saved.getId()).build();
            return Response.created(uri).entity(saved).build();
        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).
                    header("reason", "Playlist can't be null").build();
        }
    }
}

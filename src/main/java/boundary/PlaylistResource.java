package boundary;

import entity.Playlist;
import entity.Track;
import entityWrappers.LinkWrapper;
import service.CrudService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by sander on 10/28/15.
 */
@Stateless
public class PlaylistResource {

    @EJB
    PlaylistManager playlistManager;

    long id;
    CrudService manager;
    private LinkWrapper<Playlist> linkWrapper;

    public PlaylistResource() {
    }

    public PlaylistResource(long id, CrudService manager) {
        this.id = id;
        this.manager = manager;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Playlist updatePlaylistById(Playlist playlist){
        playlist.setId(id);
        return manager.update(playlist);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findPlaylistById(@Context UriInfo uriInfo){
        Playlist playlist = manager.find(Playlist.class, id);
        if( playlist != null){
            String uri = uriInfo.getAbsolutePath().toString();
            utils.Link self = new utils.Link(uri, "self");
            linkWrapper = new LinkWrapper<Playlist>().entity(playlist).link(self);
            return Response.status(Response.Status.OK).entity(linkWrapper).build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @DELETE
    public void deletePlaylistById(){
        manager.delete(Playlist.class, id);
    }


    @Path("{trackId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addTrackToPlaylist(@Context UriInfo uriInfo, @PathParam("trackId")long trackId){
        Playlist playlist = manager.find(Playlist.class, id);
        Track track = manager.find(Track.class, trackId);
        manager.addTrackToPlayList(playlist, track);
//        playlistManager = new PlaylistManager();
//        playlistManager.addTrackToPlaylist(playlist, track);

        return Response.status(Response.Status.OK).entity(playlistManager).build();
    }

//    @PUT
//    @Path("/status")
//    public Response statusUpdate(JsonObject statusUpdate){
//        if(!statusUpdate.containsKey("done")){
//            return Response.status(Response.Status.BAD_REQUEST)
//                    .header("reason", "JSON should contains field done").build();
//        }
//        boolean done = statusUpdate.getBoolean("done");
//        User user = manager.updateStatus(id, done);
//        if(user == null){
//            return Response.status(Response.Status.BAD_REQUEST).
//                    header("reason", "user with id " + id + "does not exist").build();
//        } else {
//            return Response.ok(user).build();
//        }
//    }

}

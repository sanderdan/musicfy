package boundary;

import entity.Track;
import service.CrudService;

import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * Created by sander on 10/28/15.
 */
@Stateless
public class TrackResource {
    long id;
    CrudService manager;


    public TrackResource(){

    }

    public TrackResource(long id, CrudService manager) {
        this.id = id;
        this.manager = manager;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Track updateTrackById(Track track){
        track.setId(id);
        return manager.update(track);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Track findTrackById(@Context UriInfo uriInfo){
        return manager.find(Track.class, id);
    }


    @DELETE
    public void deleteTrackById(){
        manager.delete(Track.class, id);
    }
}

package boundary;

import entity.Track;
import entityWrappers.LinkWrapper;
import service.CrudService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sander on 10/28/15.
 */
@Stateless
@Path("tracks")
public class TracksResource {

    @Inject
    CrudService manager;


    @Path("{id}")
    public TrackResource find(@PathParam("id") long id) {
        return new TrackResource(id, manager);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTracks(@Context UriInfo uriInfo) {
        List<Track> tracks =  manager.findByNamedQuery(Track.findAll);
        List<LinkWrapper> linkList = new ArrayList<>();
        for(Track t : tracks){
            String uri = uriInfo.getBaseUriBuilder()
                    .path(TracksResource.class)
                    .path(Long.toString(t.getId()))
                    .build()
                    .toString();

            utils.Link link = new utils.Link(uri, "self");
            LinkWrapper wrapper = new LinkWrapper<Track>();
            wrapper.entity(t).link(link);
            linkList.add(wrapper);
        }
        return Response.status(Response.Status.OK).entity(linkList).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(@Valid Track track, @Context UriInfo info) {
        if(track != null) {
            Track saved = this.manager.create(track);
            saved.setId(track.getId());
            URI uri = info.getAbsolutePathBuilder().path("/" + saved.getId()).build();
            return Response.created(uri).entity(saved).build();
        }
        else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).
                    header("reason", "Track can't be null").build();
        }
    }
}

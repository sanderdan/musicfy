package boundary;

import entity.Playlist;
import entity.Track;
import service.CrudServiceBean;

import javax.ejb.Stateless;

/**
 * Created by sander on 11/4/15.
 */
@Stateless
public class PlaylistManager extends CrudServiceBean {


    public Playlist addTrackToPlaylist(Playlist playlist, Track id){
        Playlist p = em.getReference(Playlist.class, playlist);
        p.getTrackList().add(id);
        em.merge(p);
        return em.merge(p);
    }
}

package boundary;

import entity.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by sander on 10/21/15.
 */
@Stateless
public class UserManager {

    @PersistenceContext
    EntityManager em;

    public User findById(long id) {
        return this.em.find(User.class, id);
    }

    public void delete(long id) {
        try {
            User user = this.em.getReference(User.class, id);
            this.em.remove(user);
        }catch (EntityNotFoundException e){
            // we want to remove it...
        }

    }

    public List<User> getUsers() {
        return this.em.createQuery(User.findAll, User.class).getResultList();
    }

    public User save(User user) {
        return this.em.merge(user);
    }

    public User updateStatus(long id, boolean done){
        User user = this.findById(id);
        if(user == null){
            return null;
        }
        user.setDone(done);
        return user;
    }


}

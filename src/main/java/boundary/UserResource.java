package boundary;

import entity.Playlist;
import entity.User;
import entityWrappers.LinkWrapper;
import service.CrudService;

import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Created by sander on 10/22/15.
 */
@Stateless
public class UserResource {

    long id;
    CrudService manager;
    LinkWrapper<User> linkWrapper;


    public UserResource(){

    }

    public UserResource(long id, CrudService manager) {
        this.id = id;
        this.manager = manager;
    }


    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public User updateUserById(User user){
        user.setId(id);
        return manager.update(user);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserById(@Context UriInfo uriInfo){

        User user = manager.find(User.class, id);
        if( user != null){
            String uri = uriInfo.getBaseUriBuilder()
                    .path(UsersResource.class)
                    .path(Long.toString(user.getId()))
                    .build()
                    .toString();
            utils.Link self = new utils.Link(uri, "self");
            linkWrapper = new LinkWrapper<User>().entity(user).link(self);
            if(!manager.findByNamedQueryWithId(Playlist.findByUserId, user.getId()).isEmpty() ) {
                uri = uriInfo.getBaseUriBuilder()
                        .path(UsersResource.class)
                        .path(Long.toString(user.getId()))
                        .path("/playlists")
                        .build()
                        .toString();
                utils.Link playlists = new utils.Link(uri, "playlists");
                linkWrapper.link(playlists);
            }

            return Response.status(Response.Status.OK).entity(linkWrapper).build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @DELETE
    public void deleteUserById(){
            manager.delete(User.class, id);
    }

//    @PUT
//    @Path("/status")
//    public Response statusUpdate(JsonObject statusUpdate){
//        if(!statusUpdate.containsKey("done")){
//            return Response.status(Response.Status.BAD_REQUEST)
//                    .header("reason", "JSON should contains field done").build();
//        }
//        boolean done = statusUpdate.getBoolean("done");
//        User user = manager.updateStatus(id, done);
//        if(user == null){
//            return Response.status(Response.Status.BAD_REQUEST).
//                    header("reason", "user with id " + id + "does not exist").build();
//        } else {
//            return Response.ok(user).build();
//        }
//    }


}

package boundary;

import entity.User;
import entityWrappers.LinkWrapper;
import service.CrudService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sander on 10/21/15.
 */
@Stateless
@Path("users")
public class UsersResource {

    private LinkWrapper<User> linkWrapper;

    @Inject
    CrudService manager;

    @Path("{id}")
    public UserResource find(@PathParam("id") long id) {
        return new UserResource(id, manager);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@Context UriInfo uriInfo, @Context Request request) {
        List<User> users =  manager.findByNamedQuery(User.findAll);
        List<LinkWrapper> linkWrappers = new ArrayList<>();
        for(User user : users){
            String uri = uriInfo.getBaseUriBuilder()
                    .path(UsersResource.class)
                    .path(Long.toString(user.getId()))
                    .build()
                    .toString();

            utils.Link link = new utils.Link(uri, "self");
            linkWrapper = new LinkWrapper<User>().entity(user).link(link);
            linkWrappers.add(linkWrapper);
        }


        return Response.status(Response.Status.OK).entity(linkWrappers).build();


    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(@Valid User user, @Context UriInfo info) {
        if(user != null) {
            User saved = this.manager.create(user);
            saved.setId(user.getId());
            URI uri = info.getAbsolutePathBuilder().path("/" + saved.getId()).build();
            return Response.created(uri).entity(saved).build();
        }
        else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).
                    header("reason", "User can't be null").build();
        }
    }
}

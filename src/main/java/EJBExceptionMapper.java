import javax.ejb.EJBException;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by sander on 10/22/15.
 */
@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {

    @Override
    public Response toResponse(EJBException e) {
        Throwable cause = e.getCause();
        Response unknownError = Response.serverError().header("cause", e.toString()).build();
        if(cause == null){
            return unknownError;
        }
            if(cause instanceof OptimisticLockException){
                Response.status(Response.Status.CONFLICT).
                        header("cause", "conflict occured: " + cause)
                        .build();
            }

        return unknownError;
    }


}

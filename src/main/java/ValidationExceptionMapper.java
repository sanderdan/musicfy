import entity.ErrorMessage;

import javax.validation.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by sander on 11/4/15.
 */
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {


    @Override
    public Response toResponse(ValidationException ex) {
        ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 3534, "Http://docs.mysite.com/api");
        if(ex instanceof ConstraintDeclarationException){
            System.out.println("ConstraintDeclarationException");
        }
        else if(ex instanceof ConstraintDefinitionException){
            System.out.println("ConstraintDefinitionException");
        }
        else if(ex instanceof ConstraintViolationException){
            System.out.println("ConstraintViolationException");
        }
        else if(ex instanceof GroupDefinitionException){
            System.out.println("GroupDefinitionException");
        }
        else{
            System.out.println("ValidationException");
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
